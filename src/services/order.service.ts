

import { Frozen } from '../shared';
import * as Sequelize from 'sequelize';
import { Model, Transaction } from 'sequelize';
import { order, bookingType, subBookingType, orderAppilanceDetail, orderErrorDetails, orderI, orderAppilanceDetailI, orderErrorDetailsI} from '../models/index';
import * as typings from '../shared/common';


@Frozen

export class OrderService {


    public createOrder = async (data: typings.orderDataI, transaction: Transaction): Promise<typings.ANY> => {

        const {
            subBookingTypeId,
            serviceProvideId: fdServiceProviderId,
            customerId

        } = data

        // const customerId = 1

        const orderObj: orderI = {
            fdServiceProviderId, customerId, subBookingTypeId
        }

        const orderData: orderI = await order.create(orderObj, { transaction })

        const orderId = orderData?.id

        // console.log("orderData",JSON.parse(JSON.stringify(orderData)));

        // const applianceobj :typings.ANY = {...applianceDetails, orderId }


        // const applianceData: orderAppilanceDetailI = await orderAppilanceDetail.create(applianceobj, { transaction })

        // const errorDetailsObj :typings.ANY = {...errorDetails, orderId }

        // const errorData:orderErrorDetailsI = await orderErrorDetails.create(errorDetailsObj,{ transaction })

        const response : typings.ANY = {
            orderData,
            // applianceData,
            // errorData
        }

        return response


    }

    public addAppliances = async (data: typings.applianceDataI, transaction: Transaction): Promise<typings.ANY> => {

        const {
            orderId,
            applianceDetails,
        } = data

        // console.log("orderData",JSON.parse(JSON.stringify(orderData)));

        const applianceobj :typings.ANY = {...applianceDetails, orderId }

        const applianceData: orderAppilanceDetailI = await orderAppilanceDetail.create(applianceobj, { transaction })

        const response : typings.ANY = {
            applianceData
        }

        return response
    }

    public addErrorData = async (data: typings.orderDataI, transaction: Transaction): Promise<typings.ANY> => {

        const {
            orderId,
            errorDetails,

        } = data

        const customerId = 1

        // console.log("orderData",JSON.parse(JSON.stringify(orderData)));

        const errorDetailsObj :typings.ANY = {...errorDetails, orderId }

        const errorData:orderErrorDetailsI = await orderErrorDetails.create(errorDetailsObj,{ transaction })

        const response : typings.ANY = {
            errorData
        }

        return response


    }

}