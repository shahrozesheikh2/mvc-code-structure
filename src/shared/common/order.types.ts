import { orderAppilanceDetailI, } from 'src/models';
import { ANY } from '.';


export interface customerDetailsI  {
   
}

// export interface orderAppilanceDetailI{
//      id?: number;
//     applianceNum?: number;
//     applianceTypeId?: number;
//     brandId?: number;
//     modelNumber?:string;
//     serialNumber?:string;
//     indexNumber?: number;
//     modelYear?: number;
//     contract?:string;
//     IsWarrenty?:boolean;
//     warrantyContractNum?:number;
//     warrentyDetails?:string;
//     warrentyNotes?:string;
//     installation?:string
//     levelUsage?:number;
//     purchaseDate?:Date;
//     priceNew?:number;
//     problem?:string;
//     errorCode?:string;
//     detail?:string;
//     notes?:string;
//     recallInfo?:string;
//     other?:string;
//     orderId?:number;
//     typeName?:string;
//     createdAt?: Date;
//     updatedAt?: Date;
//     deletedAt?: Date;
//     createdBy?: number;
//     updatedBy?: number;
//     deletedBy?: number;
 
// }
export interface orderErrorDetailsI{
    id?: number;
    name: string;
    problem: string;
    errorCode: string;
    attachmentId: number;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
    createdBy?: number;
    updatedBy?: number;
    deletedBy?: number;
    orderId?:number;

}


export interface orderDataI{
    subBookingTypeId?:number,
    serviceProvideId?:number,
    customerId?:number,
}

export interface applianceDataI{
    orderId?:number,
    applianceDetails?:orderAppilanceDetailI
}

export interface errorDataI{
    orderId?:number,
    applianceDetails?:orderAppilanceDetailI
}


// export interface orderDataI{
//     subBookingTypeId?:number,
//     serviceProvideId?:number,
//     customerDetails?:customerDetailsI,
//     applianceDetails?:orderAppilanceDetailI,
//     errorDetails?:orderErrorDetailsI

// }