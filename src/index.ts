// // import './pre-start'; // Must be the first import
// import app from '@server';
// import logger from '@shared/Logger';


// // Start the server
// const port = Number(process.env.PORT || 3000);
// console.log(
//     "port",port
// )
// app.listen(port, () => {
//     logger.info('Express server started on port: ' + port);
// });
/**
 * Module dependencies.
 */
 import * as chalk from 'chalk';
//  import * as timeout from 'connect-timeout'; // Express v4
//  import * as cors from 'cors';
 import * as dotenv from 'dotenv';
 import express from 'express';
 import * as helmet from 'helmet';
 import * as http from 'http';
 import * as httpLogs from 'morgan';
//  import * as xss from 'xss-clean';
 
//  import { sequelize } from './config/database';
 import { routes } from './routes/routes';
//  import { errorHandler, logger } from './utils';
 
 // tslint:disable-next-line: no-require-imports
 // require('./cron-jobs/appointment.jobs');
 
 
 
 /**
  * Load environment variables from .env file, where API keys and passwords are configured.
  */
 dotenv.config({ path: '.env' });
 
 /**
  * Create Express server.
  */
 const app: express.Express = express();
 
 /**
  * Timeout confg
  */
//  app.use(timeout('12000000'));
//  app.use(haltOnTimedout);
 
//  function haltOnTimedout(req: express.Request, _res: express.Response, next: express.NextFunction): void {
//    if (!req.timedout) { next(); }
//  }
 
 /**
  * Express configuration.
  */
 app.set('host', process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0');
 app.set('port', process.env.PORT || 8081);
 app.set('env', process.env.NODE_ENVR || 'development');
//  console.log(
//     "port",port
// )
//  app.use(httpLogs('dev'));
 app.use(express.urlencoded({ extended: true }));
 app.use(express.json());
//  app.use(helmet());
//  app.use(xss());
 /**
  * CORS enable
  */
//  app.use(cors());
 
 /**
  * Routes.
  */
 app.use('/api', routes);
 
 /**
  * Error Handler.
  */
//  app.use(errorHandler);
 
 export const server: http.Server = http.createServer(app);
 /**
  * Start Express server.
  */
 
 server.listen(app.get('port'), (): void => {
   // tslint:disable-next-line: no-console
   console.log(' App is running at http://localhost:%d in %s mode', app.get('port'), app.get('env'));
   // tslint:disable-next-line: no-console
   console.log('  Press CTRL-C to stop\n');
 });
 