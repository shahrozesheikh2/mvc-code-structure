
import {
    orderService // # import_service
} from '../services';

import { OrderController } from './order.controller'; // # import_controller



export const orderController: OrderController = new OrderController(orderService);
