import { NextFunction, Request, RequestHandler, Response } from 'express';
// import { sequelize } from '../config/database';


import { Frozen } from '../shared';
import { ANY } from '../shared/common';


import { OrderService } from '../services/order.service';
import { sequelize } from 'src/config/database';
import { orderSchema } from '@shared/common/validators/order.validator';



@Frozen
export class OrderController {

    /**
     *
     * @param __service
     */
     public constructor(
        public __service: OrderService
    ) { }

    /**
     *
     * @param req
     * @param res
     * @param next
     */
     public createOrder = async (req: Request, res: Response)=>{

        try{
            const orderTransaction = await sequelize.transaction();
            try {
            
                const { body } = req;

                const orderData = await orderSchema.validateAsync(body)
    
                const response: ANY = await this.__service.createOrder(orderData,orderTransaction);
    
                await orderTransaction.commit();
    
                res.status(200).json({
                    message: "order Create Succesfully",
                    result: response 
                });
    
            } catch (error:any) {
                if (orderTransaction) {orderTransaction.rollback(); }
                res.status(406).send({
                    message: error.message
                })
            }

        }catch(error){
            console.log(error);
            res.status(406).send({
                message: error
            })
        }

       
       
    }

    /**
     * @param req
     * @param res
     * @param next 
     */
    public addAppliances = async (req: Request, res: Response)=>{

        try{
            const orderTransaction = await sequelize.transaction();
            try {
            
                const { body } = req;

                const orderData = await orderSchema.validateAsync(body)
    
                const response: ANY = await this.__service.addAppliances(orderData,orderTransaction);
    
                await orderTransaction.commit();
    
                res.status(200).json({
                    message: "order Create Succesfully",
                    result: response 
                });
    
            } catch (error:any) {
                if (orderTransaction) {orderTransaction.rollback(); }
                res.status(406).send({
                    message: error.message
                })
            }

        }catch(error){
            console.log(error);
            res.status(406).send({
                message: error
            })
        }

       
       
    }

    
}

