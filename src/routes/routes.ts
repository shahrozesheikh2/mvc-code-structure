import { Router, Request, Response, } from 'express';
import { sequelize } from '../config/database';




import { orderRouter } from './order.routes';


// User-route
// const userRouter = Router();
// userRouter.get('/all', getAllUsers);
// userRouter.post('/add', addOneUser);
// userRouter.put('/update', updateOneUser);
// userRouter.delete('/delete/:id', deleteOneUser);


// Export the base-router
export const routes: Router = Router();
// const routes = Router();
routes.use('/order', orderRouter);
// export default baseRouter;

routes.use('/health-checked', (req: Request, res: Response) => {
    sequelize.authenticate()
    // tslint:disable-next-line: no-magic-numbers
    .then(() => res.status(200).send('Database connected successfully!')).catch((error) => {
        // tslint:disable-next-line: no-magic-numbers
        res.status(500).send('Database authenticaion error...');
    });
});
