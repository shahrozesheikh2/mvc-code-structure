import { NextFunction, Request, RequestHandler, Response, Router } from 'express';

// import { getAllUsers, addOneUser, updateOneUser, deleteOneUser } from './sers';

import { orderController } from '../controllers';


/**
 * User route handlers.
 */

 export const orderRouter: Router = Router();

 orderRouter.post('/', (...args: [Request, Response]) => orderController.createOrder(...args));
 orderRouter.post('/addAppliancesData', (...args: [Request, Response]) => orderController.addAppliances(...args));



//  userRouter.get('/all', getAllUsers);