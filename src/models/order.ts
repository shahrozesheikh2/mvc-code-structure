import { AutoIncrement, Column, Model, PrimaryKey, Table } from "sequelize-typescript";

export interface orderI {
    id?: number;
    fdServiceProviderId?: number;
    customerId?: number;
    subBookingTypeId?: number;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
    createdBy?: number;
    updatedBy?: number;
    deletedBy?: number;
}

@Table({
    modelName: 'order',
    tableName: 'order',
    timestamps: true
})

export class order extends Model<orderI>{
    
    @PrimaryKey
    @AutoIncrement
    @Column
    public id: number;

    @Column
    public fdServiceProviderId: number;

    @Column
    public customerId: number;

    @Column
    public subBookingTypeId: number;

    @Column
    public createdAt: Date;

    @Column
    public createdBy: number;

    @Column
    public deletedAt: Date;

    @Column
    public deletedBy: number;


    @Column
    public updatedAt: Date;

    @Column
    public updatedBy: number;


}