import { ANY } from '../shared/common';
import { 
    order , 
    subBookingType,
    bookingType ,
    orderErrorDetails,
    orderAppilanceDetail,
    recomendations } from '.';

export * from './order';
export * from './bookingType';
export * from './subBookingType';
export * from './orderAppilanceDetail'
export * from './orderErrorDetails'
export * from './recomendations'






type ModelType = ANY;

export const models: ModelType = [
    order,
    subBookingType,
    bookingType,
    orderErrorDetails,
    orderAppilanceDetail,
    recomendations
]