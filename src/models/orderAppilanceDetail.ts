import { AutoIncrement, Column, Model, PrimaryKey, Table } from "sequelize-typescript";

export interface orderAppilanceDetailI {
    id?: number;
    applianceNum?: number;
    applianceTypeId?: number;
    brandId?: number;
    modelNumber?:string;
    serialNumber?:string;
    indexNumber?: number;
    modelYear?: number;
    contract?:string;
    IsWarrenty?:boolean;
    warrantyContractNum?:number;
    warrentyDetails?:string;
    warrentyNotes?:string;
    installation?:string
    levelUsage?:number;
    purchaseDate?:Date;
    priceNew?:number;
    problem?:string;
    errorCode?:string;
    detail?:string;
    notes?:string;
    recallInfo?:string;
    other?:string;
    orderId?:number;
    typeName?:string;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
    createdBy?: number;
    updatedBy?: number;
    deletedBy?: number;
}

@Table({
    modelName: 'orderAppilanceDetail',
    tableName: 'orderAppilanceDetail',
    timestamps: true
})

export class orderAppilanceDetail extends Model<orderAppilanceDetailI>{
    
    @PrimaryKey
    @AutoIncrement
    @Column
    public id: number;

    @Column
    public applianceNum: number;

    @Column
    public applianceTypeId: number;

    @Column
    public brandId: number;
    @Column
    public modelNumber: string;

    @Column
    public serialNumber: string;

    @Column
    public indexNumber: number;

    @Column
    public modelYear: number;
    @Column
    public contract: string;
    
    @Column
    public IsWarrenty: boolean;

    @Column
    public warrantyContractNum: number;

    @Column
    public warrentyDetails: string;

    @Column
    public warrentyNotes: string;

    @Column
    public installation: string;

    @Column
    public levelUsage: number;

    @Column
    public purchaseDate: Date;

    @Column
    public priceNew: number;


    @Column
    public problem: string;

    @Column
    public errorCode: string;

    @Column
    public detail: string;

    @Column
    public notes: string;

    @Column
    public recallInfo: string;

    @Column
    public other: string;

    @Column
    public orderId: number;

    @Column
    public createdAt: Date;

    @Column
    public createdBy: number;

    @Column
    public deletedAt: Date;

    @Column
    public deletedBy: number;


    @Column
    public updatedAt: Date;

    @Column
    public updatedBy: number;


}