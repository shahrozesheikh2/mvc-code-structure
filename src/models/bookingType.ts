import { AutoIncrement, Column, Model, PrimaryKey, Table } from "sequelize-typescript";

export interface bookingTypeI {
    id: number;
    typeName:string;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
    createdBy?: number;
    updatedBy?: number;
    deletedBy?: number;
}

@Table({
    modelName: 'bookingType',
    tableName: 'bookingType',
    timestamps: true
})

export class bookingType extends Model<bookingTypeI>{
    
    @PrimaryKey
    @AutoIncrement
    @Column
    public id?: number;

    @Column
    public typeName?: string;

    @Column
    public createdAt?: Date;

    @Column
    public createdBy?: number;

    @Column
    public deletedAt?: Date;

    @Column
    public deletedBy?: number;


    @Column
    public updatedAt?: Date;

    @Column
    public updatedBy?: number;


}