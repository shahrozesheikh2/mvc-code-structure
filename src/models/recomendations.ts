import { AutoIncrement, Column, Model, PrimaryKey, Table } from "sequelize-typescript";

export interface recomendationsI {
    id?: number;
    orderId:number;
    recomendations: string;
    isVeryUrgent: Boolean;
    isNoParking: Boolean;
    isMoreTime: Boolean;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
    createdBy?: number;
    updatedBy?: number;
    deletedBy?: number;
}

@Table({
    modelName: 'recomendations',
    tableName: 'recomendations',
    timestamps: true
})

export class recomendations extends Model<recomendationsI>{
    
    @PrimaryKey
    @AutoIncrement
    @Column
    public id: number;
    
    @Column
    public recomendations: string;

    @Column
    public isVeryUrgent: Boolean;

    @Column
    public isNoParking: Boolean;

    @Column
    public isMoreTime: Boolean;

    @Column
    public orderId: number;

    @Column
    public createdAt: Date;

    @Column
    public createdBy: number;

    @Column
    public deletedAt: Date;

    @Column
    public deletedBy: number;


    @Column
    public updatedAt: Date;

    @Column
    public updatedBy: number;

}