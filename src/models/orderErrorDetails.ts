import { AutoIncrement, Column, Model, PrimaryKey, Table } from "sequelize-typescript";

export interface orderErrorDetailsI {
    id?: number;
    orderId:number;
    description: string;
    problem: string;
    errorCode: string;
    attachmentId: number;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
    createdBy?: number;
    updatedBy?: number;
    deletedBy?: number;
}

@Table({
    modelName: 'orderErrorDetails',
    tableName: 'orderErrorDetails',
    timestamps: true
})

export class orderErrorDetails extends Model<orderErrorDetailsI>{
    
    @PrimaryKey
    @AutoIncrement
    @Column
    public id: number;
    
    @Column
    public description: string;

    @Column
    public problem: string;

    @Column
    public errorCode: string;

    @Column
    public attachmentId: number;

    @Column
    public orderId: number;

    @Column
    public createdAt: Date;

    @Column
    public createdBy: number;

    @Column
    public deletedAt: Date;

    @Column
    public deletedBy: number;


    @Column
    public updatedAt: Date;

    @Column
    public updatedBy: number;

}