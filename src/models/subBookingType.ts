import { AutoIncrement, Column, Model, PrimaryKey, Table } from "sequelize-typescript";

export interface subBookingTypeI {
    id: number;
    bookingTypeId: number;
    createdAt: Date;
    updatedAt: Date;
    deletedAt: Date;
    createdBy: number;
    updatedBy: number;
    deletedBy: number;
}

@Table({
    modelName: 'subBookingType',
    tableName: 'subBookingType',
    timestamps: true
})

export class subBookingType extends Model<subBookingTypeI>{
    
    @PrimaryKey
    @AutoIncrement
    @Column
    public id: number;

    @Column
    public bookingTypeId: number;

    @Column
    public createdAt: Date;

    @Column
    public createdBy: number;

    @Column
    public deletedAt: Date;

    @Column
    public deletedBy: number;


    @Column
    public updatedAt: Date;

    @Column
    public updatedBy: number;

}